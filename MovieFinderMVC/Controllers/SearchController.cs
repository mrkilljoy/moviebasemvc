﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieFinderMVC.Models;

namespace MovieFinderMVC.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Start()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Results(string findvalue, string findparam)
        {
            ViewBag.SearchResult = GetSearchResults(findvalue, findparam);
            return View("Start");
        }

        public ActionResult MovieInfo(string Name, string Genre)
        {
            var result = GetSpecificMovie(Name, Genre);
            return View((Movie)result);
        }

        // Getting search results for displaying
        private Movie[] GetSearchResults(string txt, string param)
        {
            DataManager mng = new DataManager();
            if (param == "name")
                return mng.FindMovieByName(txt);
            else
            {
                if (param == "genre")
                    return mng.FindMovieByGenre(txt);
                else
                    return mng.FindMovieByActor(txt);
            }
        }

        private Movie GetSpecificMovie(string mName, string mGenre)
        {
            DataManager mng = new DataManager();
            return mng.GetMovie(mName, mGenre);
        }
    }
}