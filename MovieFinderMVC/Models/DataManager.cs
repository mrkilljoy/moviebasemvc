﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MovieFinderMVC.Models
{
    public class DataManager : IManager
    {
        public DataManager()
        { }

        public Movie[] FindMovieByName(string mName)
        {
            using (MovieContext mc = new MovieContext())
            {
                var result = from mv in mc.Set<Movie>() where mv.Name.Contains(mName) || mv.Name == mName select mv;

                if (result == null || result.Any() == false)
                    return null;
                else
                    return result.ToArray();
            }
        }

        public Movie[] FindMovieByGenre(string gName)
        {
            using (MovieContext mc = new MovieContext())
            {
                var result = from mv in mc.Set<Movie>() where mv.Genre.Contains(gName) || mv.Genre == gName select mv;

                if (result == null || result.Any() == false)
                    return null;
                else
                    return result.ToArray();
            }
        }

        public Movie[] FindMovieByActor(string aName)
        {
            List<Movie> movies = new List<Movie>();

            using (MovieContext mc = new MovieContext())
            {
                var actors = from act in mc.Set<Actor>() where act.Name.Contains(aName) || act.Name == aName select act;

                if (actors == null || actors.Any() == false)
                    return null;

                else
                {
                    // looking for matches for found actors
                    foreach (var a in actors)
                    {
                        var found = from mv in mc.Movies where mv.Id == a.MoiveId select mv;

                        if (found != null && found.Any() != false)
                        {
                            foreach (var item in found)
                                movies.Add(item);
                        }
                    }
                }
            }
            return movies.ToArray();
        }

        public Movie GetMovie(string mName, string mGenre)
        {
            using (MovieContext mc = new MovieContext())
            {
                var result = from mv in mc.Movies where mv.Name == mName && mv.Genre == mGenre select mv;

                if (result == null || result.Any() == false)
                    return null;
                else
                    return result.First();
            }
        }
    }
}