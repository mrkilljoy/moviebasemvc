﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieFinderMVC.Models
{
    interface IManager
    {
        Movie[] FindMovieByName(string mName);

        Movie[] FindMovieByGenre(string gName);

        Movie[] FindMovieByActor(string aName);

        Movie GetMovie(string mName, string mGenre);
    }
}
