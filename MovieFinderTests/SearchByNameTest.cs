﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieFinderMVC.Models;

namespace MovieFinderTests
{
    [TestClass]
    public class SearchByNameTest
    {
        [TestMethod]
        public void SearchByFullNameMethod()
        {
            DataManager mng = new DataManager();
            var res1 = mng.FindMovieByName("True Lies");
            var res2 = mng.FindMovieByName("Crazy Heart");
        }

        [TestMethod]
        public void SearchByNameFragmentMethod()
        {
            DataManager mng = new DataManager();
            var res1 = mng.FindMovieByName("zod");
            var res2 = mng.FindMovieByName("ade");
        }

        [TestMethod]
        public void SearchByRandomSymbolsMethod()
        {
            DataManager mng = new DataManager();
            var res1 = mng.FindMovieByName("#$534f");
            var res2 = mng.FindMovieByName("d 4rf 4ty3 $#^%$# ^");
        }
    }
}
