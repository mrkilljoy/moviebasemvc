﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieFinderMVC.Models;
using MovieFinderMVC;

namespace MovieFinderTests
{
    [TestClass]
    public class SearchByActorTest
    {
        [TestMethod]
        public void SearchByFullNameMethod()
        {
            DataManager mng = new DataManager();
            var res1 = mng.FindMovieByActor("Jeff Bridges");
            var res2 = mng.FindMovieByActor("Keira Knightley");
        }

        [TestMethod]
        public void SearchByNameFragmentMethod()
        {
            DataManager mng = new DataManager();
            var res1 = mng.FindMovieByActor("ake");
            var res2 = mng.FindMovieByActor("hornt");
        }

        [TestMethod]
        public void SearchByRandomSymbolsMethod()
        {
            DataManager mng = new DataManager();
            var res1 = mng.FindMovieByActor("#$534f");
            var res2 = mng.FindMovieByActor("d 4rf 4ty3 $#^%$# ^");
        }
    }
}
